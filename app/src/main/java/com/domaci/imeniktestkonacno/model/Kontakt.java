package com.domaci.imeniktestkonacno.model;

import com.domaci.imeniktestkonacno.db.KontaktContract;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

import static com.domaci.imeniktestkonacno.db.KontaktContract.BrojTelefona.TABLE_NAME_BROJ;
import static com.domaci.imeniktestkonacno.db.KontaktContract.Kontakt.CONTENT_URI_PATH;
import static com.domaci.imeniktestkonacno.db.KontaktContract.Kontakt.FIELD_KONTAKT_ADRESA;
import static com.domaci.imeniktestkonacno.db.KontaktContract.Kontakt.FIELD_KONTAKT_IME;
import static com.domaci.imeniktestkonacno.db.KontaktContract.Kontakt.FIELD_KONTAKT_PREZIME;
import static com.domaci.imeniktestkonacno.db.KontaktContract.Kontakt.FIELD_KONTAKT_SLIKA;
import static com.domaci.imeniktestkonacno.db.KontaktContract.Kontakt.MIMETYPE_NAME;
import static com.domaci.imeniktestkonacno.db.KontaktContract.Kontakt.MIMETYPE_TYPE;
import static com.domaci.imeniktestkonacno.db.KontaktContract.Kontakt.TABLE_NAME_KONTAKT;

@DatabaseTable(tableName = TABLE_NAME_KONTAKT)
    public class Kontakt {


        @DatabaseField(columnName = KontaktContract.Kontakt.FIELD_KONTAKT_ID, generatedId = true)
        private int id;
        @DatabaseField(columnName = FIELD_KONTAKT_SLIKA)
        private String slika;
        @DatabaseField(columnName = FIELD_KONTAKT_IME)
        private String ime;
        @DatabaseField(columnName = FIELD_KONTAKT_PREZIME)
        private String prezime;
        @DatabaseField(columnName = FIELD_KONTAKT_ADRESA)
        private String adresa;
        @ForeignCollectionField(columnName = TABLE_NAME_BROJ, eager = true)
        private ForeignCollection<BrojTelefona> brojeviTelefona;

        public Kontakt() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getSlika() {
            return slika;
        }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public String getIme() {
            return ime;
        }

        public void setIme(String ime) {
            this.ime = ime;
        }

        public String getPrezime() {
            return prezime;
        }

        public void setPrezime(String prezime) {
            this.prezime = prezime;
        }

        public String getAdresa() {
            return adresa;
        }

        public void setAdresa(String adresa) {
            this.adresa = adresa;
        }

        public ForeignCollection<BrojTelefona> getBrojeviTelefona() {
            return brojeviTelefona;
        }

        public void setBrojeviTelefona(ForeignCollection<BrojTelefona> brojeviTelefona) {
            this.brojeviTelefona = brojeviTelefona;
        }

        @Override
        public String toString() {
            return ime;
        }
}
