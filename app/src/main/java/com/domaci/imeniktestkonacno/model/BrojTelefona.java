package com.domaci.imeniktestkonacno.model;

import com.domaci.imeniktestkonacno.db.KontaktContract;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation;

import static com.domaci.imeniktestkonacno.db.KontaktContract.BrojTelefona.FIELD_BROJ_BROJ;
import static com.domaci.imeniktestkonacno.db.KontaktContract.BrojTelefona.FIELD_BROJ_TIP;
import static com.domaci.imeniktestkonacno.db.KontaktContract.BrojTelefona.TABLE_NAME_BROJ;
import static com.domaci.imeniktestkonacno.db.KontaktContract.Kontakt.TABLE_NAME_KONTAKT;

@DatabaseTable(tableName = TABLE_NAME_BROJ)
public class BrojTelefona {

    @DatabaseField(columnName = KontaktContract.BrojTelefona.FIELD_BROJ_ID, generatedId = true)
    private int id;
    @DatabaseField(columnName = FIELD_BROJ_BROJ)
    private String broj;
    @DatabaseField(columnName = FIELD_BROJ_TIP)
    private String tip;
    @DatabaseField(columnName = TABLE_NAME_KONTAKT, foreign = true, foreignAutoRefresh = true)
    private Kontakt kontakt;

    public BrojTelefona() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBroj() {
        return broj;
    }

    public void setBroj(String broj) {
        this.broj = broj;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Kontakt getKontakt() {
        return kontakt;
    }

    public void setKontakt(Kontakt kontakt) {
        this.kontakt = kontakt;
    }
}

