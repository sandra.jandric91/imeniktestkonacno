package com.domaci.imeniktestkonacno.db;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class KontaktContract {

    public static final String DATABASE_NAME = "contacts.db";
    public static final int DATABASE_VERSION = 1;

    public static class Kontakt implements BaseColumns {

        public static final String AUTHORITY = "com.domaci.imeniktestkonacno";

        public static final String TABLE_NAME_KONTAKT = "kontakt";
        public static final String FIELD_KONTAKT_ID = "id";
        public static final String FIELD_KONTAKT_SLIKA = "slika";
        public static final String FIELD_KONTAKT_IME = "ime";
        public static final String FIELD_KONTAKT_PREZIME = "prezime";
        public static final String FIELD_KONTAKT_ADRESA = "adresa";

        public static final String CONTENT_URI_PATH = TABLE_NAME_KONTAKT;
        public static final String MIMETYPE_NAME = AUTHORITY + ".provider";
        public static final String MIMETYPE_TYPE =  TABLE_NAME_KONTAKT;

        public static final int CONTENT_URI_PATTERN_MANY = 1;
        public static final int CONTENT_URI_PATTERN_ONE = 2;

        public static final Uri contentUri = new Uri.Builder()
                .scheme(ContentResolver.SCHEME_CONTENT)
                .authority(AUTHORITY)
                .appendPath(CONTENT_URI_PATH).build();
    }

    public static class BrojTelefona implements BaseColumns {

        public static final String AUTHORITY = "com.domaci.imeniktestkonacnoBR";

        public static final String TABLE_NAME_BROJ = "brojevi";
        public static final String FIELD_BROJ_ID = "id";
        public static final String FIELD_BROJ_BROJ = "broj";
        public static final String FIELD_BROJ_TIP = "tip";

        public static final String CONTENT_URI_PATH_BR = TABLE_NAME_BROJ;
        public static final String MIMETYPE_NAME = AUTHORITY + ".providerBR";
        public static final String MIMETYPE_TYPE =  TABLE_NAME_BROJ;

        public static final int CONTENT_URI_PATTERN_MANY_BR = 11;
        public static final int CONTENT_URI_PATTERN_ONE_BR = 22;

        public static final Uri contentUri = new Uri.Builder()
                .scheme(ContentResolver.SCHEME_CONTENT)
                .authority(AUTHORITY)
                .appendPath(CONTENT_URI_PATH_BR).build();
    }
}
