package com.domaci.imeniktestkonacno.activities;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.domaci.imeniktestkonacno.AboutDijalog;
import com.domaci.imeniktestkonacno.App;
import com.domaci.imeniktestkonacno.model.BrojTelefona;
import com.domaci.imeniktestkonacno.db.DatabaseHelper;
import com.domaci.imeniktestkonacno.model.Kontakt;
import com.domaci.imeniktestkonacno.R;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageView imageView;
    private EditText etImeDetalji;
    private EditText etPrezimeDetalji;
    private EditText etAdresaDetalji;
    private Spinner spinner;
    private EditText etBroj;
    private EditText etTip;

    private Button btnDodajBroj;
    private Button btnObrisiKontakt;
    private Button btnIzmeniKontakt;

    private List<BrojTelefona> telefoniLista;
    private ArrayAdapter adapter;

    private Kontakt kontakt;
    private BrojTelefona brojTelefona = null;
    private DatabaseHelper databaseHelper;

    private AlertDialog aboutDijalog;
    private SharedPreferences sharedPreferences;
    public static String TOAST_SETTINGS = "toast_settings_cb";
    public static String NOTIF_SETTINGS = "notif_settings_cb";
    public static final int NOTIF_ID = 1;

    private static final int SELECT_PICTURE = 0;
    private String picturePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setupToolbar();
        dodajTip();
        showList();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void dodajTip() {
        brojTelefona = new BrojTelefona();
        brojTelefona.setTip("Fiksni telefon");

        try {
            getDatabaseHelper().getBrojTelefonaDao().create(brojTelefona);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        brojTelefona = new BrojTelefona();
        brojTelefona.setTip("Mobilni telefon");

        try {
            getDatabaseHelper().getBrojTelefonaDao().create(brojTelefona);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        brojTelefona = new BrojTelefona();
        brojTelefona.setTip("Poslovni telefon");

        try {
            getDatabaseHelper().getBrojTelefonaDao().create(brojTelefona);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void showList() {

        imageView = findViewById(R.id.imageView);
        etImeDetalji = findViewById(R.id.etImeDetalji);
        etPrezimeDetalji = findViewById(R.id.etPrezimeDetalji);
        etAdresaDetalji = findViewById(R.id.etAdresaDetalji);
        etBroj = findViewById(R.id.etBroj);
        etTip = findViewById(R.id.etTip);
        spinner = findViewById(R.id.spinnerKategorija);
        setupAdapter();

        btnDodajBroj = findViewById(R.id.btnDodajBroj);
        btnObrisiKontakt = findViewById(R.id.btnObrisiKontakt);
        btnIzmeniKontakt = findViewById(R.id.btnIzmeniKontakt);

        btnDodajBroj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tip = String.valueOf(spinner.getSelectedItem());
                etTip.setText(tip);
                etBroj.setText(etBroj.getText().toString());
                if (brojTelefona != null) {
                    brojTelefona.setTip(etTip.getText().toString());
                    brojTelefona.setBroj(etBroj.getText().toString());
                    try {
                        getDatabaseHelper().getBrojTelefonaDao().update(brojTelefona);
                        Log.v("BBBB", "broj unet u bazu");
                        showToast("Broj dodat");
                        showNotif("Broj dodat", getApplicationContext());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        int id = getIntent().getExtras().getInt("kontaktID");

        try {
            kontakt = getDatabaseHelper().getKontaktDao().queryForId(id);
            brojTelefona = getDatabaseHelper().getBrojTelefonaDao().queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (kontakt.getSlika() != null) {
            imageView.setImageBitmap(BitmapFactory.decodeFile(kontakt.getSlika()));
        }
        etImeDetalji.setText(kontakt.getIme());
        etPrezimeDetalji.setText(kontakt.getPrezime());
        etAdresaDetalji.setText(kontakt.getAdresa());
        etTip.setText(brojTelefona.getTip());
        etBroj.setText(brojTelefona.getBroj());

        btnObrisiKontakt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (kontakt != null) {
                    try {
                        getDatabaseHelper().getKontaktDao().delete(kontakt);
                        showToast("Kontakt obrisan");
                        showNotif("Kontakt obrisan", getApplicationContext());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                onBackPressed();
            }
        });

        btnIzmeniKontakt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (kontakt != null) {
                    kontakt.setIme(etImeDetalji.getText().toString());
                    kontakt.setPrezime(etPrezimeDetalji.getText().toString());
                    kontakt.setAdresa(etAdresaDetalji.getText().toString());
                    kontakt.setSlika(picturePath);
                    Log.v("slika", "slika setovana za kontakt");

                    try {
                        getDatabaseHelper().getKontaktDao().update(kontakt);
                        showToast("Kontakt izmenjen");
                        showNotif("Kontakt izmenjen", getApplicationContext());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                onBackPressed();
            }
        });
    }

    private void setupAdapter() {
        try {
            telefoniLista = getDatabaseHelper().getBrojTelefonaDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (telefoniLista == null) {
            telefoniLista = new ArrayList<>(telefoniLista);
        }

        spinner = findViewById(R.id.spinnerKategorija);
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.tip_telefona));
        spinner.setAdapter(adapter);
        String [] data = getResources().getStringArray(R.array.tip_telefona);
        for (int i = 0; i < data.length; i++) {
            if (data[i].equalsIgnoreCase(brojTelefona.getTip())) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    private void showToast(String message) {
        boolean toast = sharedPreferences.getBoolean(TOAST_SETTINGS, false);
        if (toast) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showNotif(String message, Context context) {
        boolean notif = sharedPreferences.getBoolean(NOTIF_SETTINGS, false);
        if (notif) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, App.CHANNEL_ID);
            builder.setSmallIcon(R.drawable.ic_action_settings);
            builder.setContentTitle(message);
            builder.setContentText(message);
            notificationManager.notify(NOTIF_ID, builder.build());
        }
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.slika:
                showSlika();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
            showSlika();
        }
    }

    private void showSlika() {
        if (isStoragePermissionGranted()) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, SELECT_PICTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
       if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && data != null) {
           Uri selectedImage = data.getData();
           String [] filePathColumn = {MediaStore.Images.Media.DATA};

           if (selectedImage != null) {
               Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
               if (cursor != null) {
                   cursor.moveToFirst();

                   int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                   picturePath = cursor.getString(columnIndex);
                   cursor.close();
                   imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
               }
           }
       }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
