package com.domaci.imeniktestkonacno.activities;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.domaci.imeniktestkonacno.AboutDijalog;
import com.domaci.imeniktestkonacno.db.DatabaseHelper;
import com.domaci.imeniktestkonacno.db.KontaktContract;
import com.domaci.imeniktestkonacno.model.Kontakt;
import com.domaci.imeniktestkonacno.R;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private Toolbar toolbar;

    private EditText etIme;
    private EditText etPrezime;
    private EditText etAdresa;
    private Button btnOk;
    private Button btnCancel;

    private ArrayAdapter<Kontakt> adapter;
    private ListView listView;
    private List<Kontakt> kontaktiLista;

    private DatabaseHelper databaseHelper;

    private AlertDialog aboutDijalog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();
        setList();
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    private void setList() {

        try {
            kontaktiLista = getDatabaseHelper().getKontaktDao().queryForAll();
            refresh();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (kontaktiLista == null) {
            kontaktiLista = new ArrayList<>();
        }

        listView = findViewById(R.id.list_view);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, kontaktiLista);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                int kontaktID = adapter.getItem(position).getId();
                intent.putExtra("kontaktID", kontaktID);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.settings:
                showSettings();
                break;
            case R.id.about:
                showDijalog();
                break;
            case R.id.action_add:
                dodajKontakt();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDijalog() {
        if (aboutDijalog == null) {
            aboutDijalog = new AboutDijalog(this).prepareDialog();
        } else {
            if (aboutDijalog.isShowing()) {
                aboutDijalog.dismiss();
            }
        }
        aboutDijalog.show();
    }

    private void showSettings() {
        Intent intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    private void dodajKontakt() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dodaj_kontakt);
        dialog.setCancelable(false);

        etIme = dialog.findViewById(R.id.etIme);
        etPrezime = dialog.findViewById(R.id.etPrezime);
        etAdresa = dialog.findViewById(R.id.etAdresa);

        btnCancel = dialog.findViewById(R.id.btnCancel);
        btnOk = dialog.findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ime = etIme.getText().toString();
                String prezime = etPrezime.getText().toString();
                String adresa = etAdresa.getText().toString();

                Kontakt kontakt = new Kontakt();

                kontakt.setIme(ime);
                kontakt.setPrezime(prezime);
                kontakt.setAdresa(adresa);

                kontaktiLista.add(kontakt);

                try {
                    getDatabaseHelper().getKontaktDao().create(kontakt);
                    refresh();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void refresh() {
        if (listView != null) {
            adapter = (ArrayAdapter<Kontakt>) listView.getAdapter();
            if (adapter != null) {
                try {
                    adapter.clear();
                    kontaktiLista = getDatabaseHelper().getKontaktDao().queryForAll();
                    adapter.addAll(kontaktiLista);
                    adapter.notifyDataSetChanged();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        refresh();
        super.onResume();
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}