package com.domaci.imeniktestkonacno;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

public class AboutDijalog extends AlertDialog.Builder {

    public AboutDijalog(@NonNull Context context) {
        super(context);
        setTitle("About");
        setMessage("APLIKACIJA ImenikTEST \n Autor: Sandra");
        setPositiveButton("U redu", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        setNegativeButton("Odustani", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    public AlertDialog prepareDialog() {
        AlertDialog alertDialog = create();
        alertDialog.setCanceledOnTouchOutside(false);
        return alertDialog;
    }
}
